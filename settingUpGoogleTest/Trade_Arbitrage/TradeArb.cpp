//
// Created by evgeniy on 04.12.16.
//
#include <sstream>
#include "TradeArb.h"

bool graph_methods::checkArbitrage(vector<edge> &e, int count) {
    vector<double> moneyInVertex(count);

    int intInfinity = std::numeric_limits<int>::infinity();

    for (int i = 0; i < count; ++i)
        moneyInVertex.push_back(intInfinity);
    moneyInVertex[1] = 1;

    int count_loops = 0;

    bool operationAbility = true;
    while (operationAbility) {
        operationAbility = false;
        ++count_loops;
        for (int j = 0; j < e.size(); ++j) {
            if ((e[j].cost != 0) && (e[j].cost != -1)) {
                if (moneyInVertex[e[j].b] < moneyInVertex[e[j].a] * e[j].cost) {
                    moneyInVertex[e[j].b] = moneyInVertex[e[j].a] * e[j].cost;
                    operationAbility = true;
                }
            }
        }
        if (count_loops > count * e.size())
            return true;
    }
    return false;
}

void graph_methods::FillGraph(vector<edge> &graph, int count, string &input) {
    std::stringstream test_stream;
    test_stream << input;
    int shift = count;
    for (int i = 0; i < count; ++i) {
        edge tmp;
        for (int j = 0; j < count; ++j) {
            tmp.a = i;
            tmp.b = j;
            if (count == shift) {   //т.е. время сдвига(при вводе пустая клетка)
                tmp.cost = 1;
                graph.push_back(tmp);
                shift = 0;
                continue;
            }

            test_stream >> tmp.cost;

            graph.push_back(tmp);
            ++shift;
        }
    }
}


