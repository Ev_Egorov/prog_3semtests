//
// Created by evgeniy on 04.12.16.
//

#ifndef SETTINGUPGOOGLETEST_CLASSNAME_H
#define SETTINGUPGOOGLETEST_CLASSNAME_H

#include <vector>
#include <string>
#include <limits>
#include <iosfwd>
#include <iostream>

using std::vector;
using std::string;
using std::cin;
using std::cout;

struct edge {
    int a, b;
    double cost;
};

class graph_methods {
public:
    bool checkArbitrage(vector<edge>&, int);
    void FillGraph(vector<edge>&, int, string&);
};

#endif //SETTINGUPGOOGLETEST_CLASSNAME_H
