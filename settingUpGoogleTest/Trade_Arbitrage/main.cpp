#include <gtest/gtest.h>
#include "TradeArb.h"

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    if (RUN_ALL_TESTS() != 0)
        return 1;

    int count;
    vector<edge> graph;
    cin >> count;
    string input, tmp;
    int i=0;
    while (i++ < count) {
        cin >> tmp;
        input += ' ' + tmp;
    }

    graph_methods action;
    action.FillGraph(graph, count, input);
    cout << ((action.checkArbitrage(graph, count)) ? "YES" : "NO");

    return 0;
}