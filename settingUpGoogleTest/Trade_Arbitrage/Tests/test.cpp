//
// Created by evgeniy on 04.12.16.
//
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "../TradeArb.h"

using testing::Eq;

namespace {
    class GraphMaintain : public testing::Test {
    public:
        graph_methods action;
    };
}


TEST_F(GraphMaintain, test_algo) {
    bool ans;
    int vertex_count = 4;
    vector<edge> graph(vertex_count * vertex_count);

    std::string input = "32.1  1.50 78.66 0.03 0.04  2.43 0.67 21.22 51.89 0.01 -1  0.02";

    action.FillGraph(graph, vertex_count, input);
    ans = action.checkArbitrage(graph, vertex_count);
    EXPECT_EQ(ans, true);
}

TEST_F(GraphMaintain, test_algo2) {
    bool ans;
    int vertex_count = 2;
    vector<edge> graph(vertex_count * vertex_count);

    std::string input = "10.0 0.09";
    action.FillGraph(graph, vertex_count, input);
    ans = action.checkArbitrage(graph, vertex_count);
    EXPECT_EQ(ans, false);
}
